#!/bin/bash

set -e
set -x

PROJECTS=(
  "cert-manager-webhook-inwx"
)

mkdir -p cache

for project in "${PROJECTS[@]}"; do
  mkdir -p public/$project
  wget https://smueller18.gitlab.io/$project/content.tar.gz -O cache/$project.tar.gz
  tar xvf cache/$project.tar.gz -C public/$project
done

find public -name index.yaml -delete

helm repo index public --url ${CI_PAGES_URL:-localhost}
